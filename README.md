# Heurist

![release badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/heurist/-/badges/release.svg)
![pipeline badge](https://gitlab.ub.uni-bielefeld.de/sfb1288inf/heurist/badges/main/pipeline.svg?ignore_skipped=true)

[Heurist](https://heuristnetwork.org/) is a research-driven data management system that puts you in charge, allowing you to design, populate, explore and publish your own richly-structured database(s) within hours, through a simple web interface, without the need for programmers or consultants.

## Prerequisites and requirements

To deploy this software, Docker, Docker Compose and Git must be installed first. It is possible to publish the web service directly via a port or run it behind a reverse proxy (e.g. nginx or traefik). In the latter case this must also be installed separately in advance.

## Example usage

The following code block needs to be executed only once.

```bash
# Clone this repository
git clone https://gitlab.ub.uni-bielefeld.de/sfb1288inf/heurist.git

# Navigate into the repository directory
cd heurist

# Create configuration files from templates and edit it to suit your needs
cp db.env.tpl db.env
nano db.env
cp heurist.env.tpl heurist.env
nano heurist.env

# Create a docker-compose.override.yml configuration
# The docker-compose directory contains some examples for common use cases
```

On all subsequent executions, you only need to use the following commands.

```bash
# Start Heurist
docker compose up -d

# Stop Heurist
docker compose down
```
