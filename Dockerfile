FROM php:7.4.33-apache-bullseye


ENV LANG=C.UTF-8


##############################################################################
# Install system dependencies                                                #
##############################################################################
RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
    unzip \
    wget \
    zip \
 && rm -r /var/lib/apt/lists/*


##############################################################################
# Install PHP extensions                                                     #
##############################################################################
# https://github.com/mlocati/docker-php-extension-installer
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions \
    curl \
    exif \
    mbstring \
    memcache \
    mysqli \
    pdo \
    pdo_mysql \
    gd \
    xsl \
    zip


##############################################################################
# Configure PHP                                                              #
##############################################################################
RUN mv "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini"
COPY php.override.ini "${PHP_INI_DIR}/conf.d/php.override.ini"


##############################################################################
# Install Heurist                                                            #
##############################################################################
ENV HEURIST_VERSION="6.3.18"
RUN curl \
    --fail \
    --location \
    --output "/usr/local/bin/install_heurist.sh" \
    --show-error \
    --silent \
    "https://HeuristRef.net/HEURIST/DISTRIBUTION/install_heurist.sh" \
 && chmod +x "/usr/local/bin/install_heurist.sh" \
 && install_heurist.sh "h${HEURIST_VERSION}" \
 && chown -R www-data:www-data "/var/www/html"


##############################################################################
# Configure Heurist                                                          #
##############################################################################
ENV HEURIST_CONFIG_FILE="/var/www/html/HEURIST/heuristConfigIni.php"
ENV HEURIST_OVERRIDE_CONFIG_FILE="/var/www/html/HEURIST/heuristConfigIni.override.php"
RUN sed -i '${/?>/d;}' "${HEURIST_CONFIG_FILE}" \
 && echo "" >> "${HEURIST_CONFIG_FILE}" \
 && echo "\$override_config = \"${HEURIST_OVERRIDE_CONFIG_FILE}\";" >> "${HEURIST_CONFIG_FILE}" \
 && echo "if (is_file(\$override_config)) {" >> "${HEURIST_CONFIG_FILE}" \
 && echo "    include_once(\$override_config);" >> "${HEURIST_CONFIG_FILE}" \
 && echo "}" >> "${HEURIST_CONFIG_FILE}" \
 && echo "" >> "${HEURIST_CONFIG_FILE}" \
 && echo "?>" >> "${HEURIST_CONFIG_FILE}"
COPY --chown=nopaque:nopaque heuristConfigIni.override.php "${HEURIST_OVERRIDE_CONFIG_FILE}"


COPY docker-heurist-entrypoint /usr/local/bin


EXPOSE 80


VOLUME "/var/www/html/HEURIST/HEURIST_FILESTORE"


ENTRYPOINT ["docker-heurist-entrypoint"]
CMD ["apache2-foreground"]
