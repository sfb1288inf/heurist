<?php

function arrayFromString($string) {
  $values = explode(',', $string);
  return $values;
}

function createAssociativeArrayFromString($string) {
  // Split the string by semicolon to get individual key-value pairs
  $keyValuePairs = explode(';', $string);

  // Initialize an empty associative array
  $associativeArray = [];

  // Iterate through each key-value pair
  foreach ($keyValuePairs as $pair) {
      // Split each pair by comma to separate key and value
      $pairParts = explode(',', $pair);
      
      // Check if the pair has both key and value
      if (count($pairParts) === 2) {
          $key = $pairParts[0];
          $value = $pairParts[1];
          
          // Create an associative array with key-value pairs
          $associativeArray[$key] = $value;
      }
  }

  return $associativeArray;
}

if (getenv('HEURIST_SERVER_NAME') !== false) $serverName = getenv('HEURIST_SERVER_NAME');
if (getenv('HEURIST_MAIL_DOMAIN') !== false) $mailDomain = getenv('HEURIST_MAIL_DOMAIN');

if (getenv('HEURIST_HEURIST_REFERENCE_SERVER') !== false) $heuristReferenceServer = getenv('HEURIST_HEURIST_REFERENCE_SERVER');

if (getenv('HEURIST_DB_HOST') !== false) $dbHost = getenv('HEURIST_DB_HOST');
if (getenv('HEURIST_DB_PORT') !== false) $dbPort = getenv('HEURIST_DB_PORT');
if (getenv('HEURIST_DB_ADMIN_USERNAME') !== false) $dbAdminUsername = getenv('HEURIST_DB_ADMIN_USERNAME');
if (getenv('HEURIST_DB_ADMIN_PASSWORD') !== false) $dbAdminPassword = getenv('HEURIST_DB_ADMIN_PASSWORD');
if (getenv('HEURIST_DB_PREFIX') !== false) $dbPrefix = getenv('HEURIST_DB_PREFIX');

if (getenv('HEURIST_DEFAULT_ROOT_FILE_UPLOAD_URL') !== false) $defaultRootFileUploadURL = getenv('HEURIST_DEFAULT_ROOT_FILE_UPLOAD_URL');
if (getenv('HEURIST_DEFAULT_ROOT_FILE_UPLOAD_PATH') !== false) $defaultRootFileUploadPath = getenv('HEURIST_DEFAULT_ROOT_FILE_UPLOAD_PATH');

if (getenv('HEURIST_SYS_ADMIN_EMAIL') !== false) $sysAdminEmail = getenv('HEURIST_SYS_ADMIN_EMAIL');
if (getenv('HEURIST_INFO_EMAIL') !== false) $infoEmail = getenv('HEURIST_INFO_EMAIL');
if (getenv('HEURIST_BUG_EMAIL') !== false) $bugEmail = getenv('HEURIST_BUG_EMAIL');

if (getenv('HEURIST_PASSWORD_FOR_DATABASE_CREATION') !== false) $passwordForDatabaseCreation = getenv('HEURIST_PASSWORD_FOR_DATABASE_CREATION');
if (getenv('HEURIST_PASSWORD_FOR_DATABASE_DELETION') !== false) $passwordForDatabaseDeletion = getenv('HEURIST_PASSWORD_FOR_DATABASE_DELETION');
if (getenv('HEURIST_PASSWORD_FOR_RESERVED_CHANGES') !== false) $passwordForReservedChanges = getenv('HEURIST_PASSWORD_FOR_RESERVED_CHANGES');
if (getenv('HEURIST_PASSWORD_FOR_SERVER_FUNCTIONS') !== false) $passwordForServerFunctions = getenv('HEURIST_PASSWORD_FOR_SERVER_FUNCTIONS');

if (getenv('HEURIST_HTTP_PROXY_ALWAYS_ACTIVE') !== false) $httpProxyAlwaysActive = getenv('HEURIST_HTTP_PROXY_ALWAYS_ACTIVE') === 'true';
if (getenv('HEURIST_HTTP_PROXY') !== false) $httpProxy = getenv('HEURIST_HTTP_PROXY');
if (getenv('HEURIST_HTTP_PROXY_AUTH') !== false) $httpProxyAuth = getenv('HEURIST_HTTP_PROXY_AUTH');

if (getenv('HEURIST_INDEX_SERVER_ADRESS') !== false) $indexServerAddress = getenv('HEURIST_INDEX_SERVER_ADRESS');
if (getenv('HEURIST_INDEX_SERVER_PORT') !== false) $indexServerPort = getenv('HEURIST_INDEX_SERVER_PORT');

if (getenv('HEURIST_WEBSITE_THUMBNAIL_SERVICE') !== false) $websiteThumbnailService = getenv('HEURIST_WEBSITE_THUMBNAIL_SERVICE');
if (getenv('HEURIST_WEBSITE_THUMBNAIL_USERNAME') !== false) $websiteThumbnailUsername = getenv('HEURIST_WEBSITE_THUMBNAIL_USERNAME');
if (getenv('HEURIST_WEBSITE_THUMBNAIL_PASSWORD') !== false) $websiteThumbnailPassword = getenv('HEURIST_WEBSITE_THUMBNAIL_PASSWORD');
if (getenv('HEURIST_WEBSITE_THUMBNAIL_X_SIZE') !== false) $websiteThumbnailXsize = (int) getenv('HEURIST_WEBSITE_THUMBNAIL_X_SIZE');
if (getenv('HEURIST_WEBSITE_THUMBNAIL_Y_SIZE') !== false) $websiteThumbnailYsize = (int) getenv('HEURIST_WEBSITE_THUMBNAIL_Y_SIZE');

if (getenv('HEURIST_ALLOW_CMS_CREATION') !== false) $allowCMSCreation = (int) getenv('HEURIST_ALLOW_CMS_CREATION');

if (getenv('HEURIST_ESTC_USER_NAME') !== false) $ESTC_UserName = getenv('HEURIST_ESTC_USER_NAME');
if (getenv('HEURIST_ESTC_PASSWORD') !== false) $ESTC_Password = getenv('HEURIST_ESTC_PASSWORD');
if (getenv('HEURIST_ESTC_PERMITTED_DBS') !== false) $ESTC_PermittedDBs = getenv('HEURIST_ESTC_PERMITTED_DBS');
if (getenv('HEURIST_ESTC_SERVER_URL') !== false) $ESTC_ServerURL = getenv('HEURIST_ESTC_SERVER_URL');

if (getenv('HEURIST_ABSOLUTE_PATHS_TO_REMOVE_FROM_WEB_PAGES') !== false) $absolutePathsToRemoveFromWebPages = arrayFromString(getenv('HEURIST_ABSOLUTE_PATHS_TO_REMOVE_FROM_WEB_PAGES'));

if (getenv('HEURIST_NEED_ENCODE_RECORD_DETAILS') !== false) $needEncodeRecordDetails = (int) getenv('HEURIST_NEED_ENCODE_RECORD_DETAILS');

if (getenv('HEURIST_COMMON_LANGUAGES_FOR_TRANSLATION') !== false) $common_languages_for_translation = arrayFromString(getenv('HEURIST_COMMON_LANGUAGES_FOR_TRANSLATION'));

if (getenv('HEURIST_SAML_SERVICE_PROVIDES') !== false) $saml_service_provides = createAssociativeArrayFromString(getenv('HEURIST_SAML_SERVICE_PROVIDES'));

?>
