##############################################################################
#                                                                            #
# All settings within this file will be used in a config file which is       #
# generated during the container startup process.                            #
# For more info about what configurations are available and what they do,    #
# check your heuristConfigIni.php.                                           #
#                                                                            #
# Settings that are commented out are optional.                              #
# Settings annotated with FORMAT must be set with the following syntax:      #
# - Array: value1,value2,value3                                              #
# - AssociativeArray: key1,value1;key2,value2;key3,value3                    #
# - Boolean: true or false                                                   #
#                                                                            #
##############################################################################

# HEURIST_SERVER_NAME=
# HEURIST_MAIL_DOMAIN=
# HEURIST_HEURIST_REFERENCE_SERVER=

# NOTE: This must be the name of the database container as set in docker-compose.yml
HEURIST_DB_HOST=db
# HEURIST_DB_PORT=
HEURIST_DB_ADMIN_USERNAME=
HEURIST_DB_ADMIN_PASSWORD=
# HEURIST_DB_PREFIX=

HEURIST_DEFAULT_ROOT_FILE_UPLOAD_URL=
# HEURIST_DEFAULT_ROOT_FILE_UPLOAD_PATH=

HEURIST_SYS_ADMIN_EMAIL=
# HEURIST_INFO_EMAIL=
# HEURIST_BUG_EMAIL=

# HEURIST_PASSWORD_FOR_DATABASE_CREATION=
# HEURIST_PASSWORD_FOR_DATABASE_DELETION=
# HEURIST_PASSWORD_FOR_RESERVED_CHANGES=
# HEURIST_PASSWORD_FOR_SERVER_FUNCTIONS=

# FORMAT: Boolean
# HEURIST_HTTP_PROXY_ALWAYS_ACTIVE=
# HEURIST_HTTP_PROXY=
# HEURIST_HTTP_PROXY_AUTH=

# HEURIST_INDEX_SERVER_ADRESS=
# HEURIST_INDEX_SERVER_PORT=

# HEURIST_WEBSITE_THUMBNAIL_SERVICE=
# HEURIST_WEBSITE_THUMBNAIL_USERNAME=
# HEURIST_WEBSITE_THUMBNAIL_PASSWORD=
# HEURIST_WEBSITE_THUMBNAIL_X_SIZE=
# HEURIST_WEBSITE_THUMBNAIL_Y_SIZE=

# HEURIST_ALLOW_CMS_CREATION=

# HEURIST_ESTC_USER_NAME=
# HEURIST_ESTC_PASSWORD=
# HEURIST_ESTC_PERMITTED_DBS=
# HEURIST_ESTC_SERVER_URL=

# FORMAT: Array
# HEURIST_ABSOLUTE_PATHS_TO_REMOVE_FROM_WEB_PAGES=

# HEURIST_NEED_ENCODE_RECORD_DETAILS=

# FORMAT: Array
# HEURIST_COMMON_LANGUAGES_FOR_TRANSLATION=

# FORMAT: AssociativeArray
# HEURIST_SAML_SERVICE_PROVIDES=
